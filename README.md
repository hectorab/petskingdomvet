# Petskingdom monorepo
This is a monorepo example with a single Next.js site (./apps/app) that has installed two local packages:

./packages/ui: Exports UI components that use TypeScript and Tailwind CSS. It's transpiled by the Next.js app using next-transpile-modules
./packages/utils: Exports utility functions that use TypeScript

The monorepo is using Turborepo and pnpm workspaces to link packages together, but it can also work with yarn workspaces or npm workspaces.